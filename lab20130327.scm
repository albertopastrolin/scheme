#lang scheme
(require "tassellation.ss")
;; Chessboard
;; Claudio Mirolo, 16/03/2012

;; Per eseguire questo codice e' necessario
;; utilizzare il TeachPack "tassellation.ss"


;; Chessboard
;;
;; Hai a disposizione due procedure:
;;
;;   (chessboard-image n)
;;
;; che assume come valore l'immagine di una scacchiera n x n
;; libera, in cui non sono state collocate regine;
;;
;;   (add-queen-image i j n img)
;;
;; che assume come valore l'immagine di una scacchiera n x n,
;; nella configurazione rappresentata dal parametro  img
;; e con in piu' l'immagine di una regina nella posizione
;; individuata dalla coppia di coordinate i, j.
;; Il parametro  n  indica il numero di scacchi (quadratini)
;; lungo ciascuno dei lati della scacchiera; n deve essere
;; coerente con la dimensione della scacchiera raffigurata
;; nell'immagine img.
;;
;; Esempi:
;;
;;  (add-queen-image 1 3 4 (chessboard-image 4))
;;
;;  (add-queen-image
;;    2 1 4 (add-queen-image 1 3 4 (chessboard-image 4)))
;;
;; Realizza il dato astratto "Scacchiera"
;; sulla base del seguente suggerimento:
;;
;;   (define empty-board
;;     (lambda (n)
;;       (list  ; la rappresentazione si articola in quattro componenti:
;;        n                        ; 1) dimensione della scacchiera
;;        0                        ; 2) numero di regine collocate
;;        (lambda (i j) #f)        ; 3) predicato: posizione minacciata?
;;        (chessboard-image size)  ; 4) immagine della scacchiera
;;        )
;;       ))
;;
;; (confronta l'esempio discusso in classe).


;; Unit step

(define step-length 28)


;; Tile definitions

(set-default-image-size! (* 5 step-length))


(define chessboard-image
  (lambda (n)
    (overlay
    (squares n 0 0)
    (overlay 
      (overlay 
      (line  -1 -1  1 -1)
      (line  1 -1  1 1))
      (overlay 
      (line  1 1  -1 1)
      (line  -1 1  -1 -1))
      ))
    ))


(define add-queen-image
  (lambda (i j n board)
    (overlay
    board
    (queen n (- j 1) (- n i))
    )
    ))


(define squares
  (lambda (n i j)
    (if (and (>= i (- n 2)) (= (+ j 1) n))
        (shaded-square n i j)
        (overlay
        (shaded-square n i j)
        (if (< (+ i 2) n)
            (squares n (+ i 2) j)
            (squares n (remainder (+ j 1) 2) (+ j 1))))
        )
    ))


(define shaded-square
  (lambda (n i j)
    (let ((x (- (/ (* 2 i) n) 1))
          (y (- (/ (* 2 j) n) 1))
          (d (/ 2 n))
          )
      (overlay
      (filled-triangle  x y  (+ x d) y  (+ x d) (+ y d))
      (filled-triangle  x y  (+ x d) (+ y d)  x (+ y d))
      ))
    ))


(define queen
  (lambda (n i j)
    (let ((x (+ (- (/ (* 2 i) n) 1) (/ 1 (* 2 n))))
          (y (+ (- (/ (* 2 j) n) 1) (/ 1 (* 2 n))))
          (d (/ 1 n))
          )
      (overlay
      (overlay
        (overlay
        (overlay
          (line  (+ x (/ d 4)) y  x (+ y d))
          (line  x (+ y d)  (+ x (/ d 4) (/ d 2)) y))
        (line  (+ x (/ d 2)) y  x (+ y d)))
        (overlay
        (line  (+ x (/ d 4)) y  (+ x (/ d 2)) (+ y d))
        (line  (+ x (/ d 2)) (+ y d)  (+ x (/ d 4) (/ d 2)) y)))
      (overlay
        (overlay
        (overlay
          (line  (+ x (/ d 2)) y  (+ x (/ d 2)) (+ y d))
          (line  (+ x (/ d 4)) y  (+ x d) (+ y d)))
        (line  (+ x d) (+ y d)  (+ x (/ d 4) (/ d 2)) y))
        (overlay
        (line  (+ x (/ d 2)) y  (+ x d) (+ y d))
        (line  (+ x (/ d 4)) y  (+ x (/ d 4) (/ d 2)) y)))
      ))
    ))
