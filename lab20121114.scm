#lang scheme
;Pastrolin Alberto
;14/11/2012
;Many functions for the list

;The function belong? return true if an element is in list and return false if not
(define belong?					;return a boolean
  (lambda (list element)			;list is a list, element is an integer
    (if (null? list)				;base case, if a list is null than element can't belong to list
        false
        (if (= (car list) element)
            true
            (belong? (cdr list) element)	;recursive call
        )
    )
  )
)

;The function position return the index of an element in a list
(define position				;return an index (integer >=0) if the element is found in the list, else it return -1
  (lambda (list element)			;list is a list, element is an integer
    (if (belong? list element)			;base case
        (if (= (car list) element)		;we have found the element
            0
            (+ 1 (position (cdr list) element))	;recursive call
        )
        -1					;the element is not found in the list
    )
  )
)

;The function sorted-ins insert an element in a sorted list, and return also a sorted list
(define sorted-ins				;return a sorted list
  (lambda (list ins)				;list is a sorted list, ins is the integer that we wont to insert in the list
    (if (belong? list ins)			;a quicker control to check if the element is just in the list (in this case the list is already sorted with ins)
        list
        (if (null? list)			;another quick control, if the list hasn't elements than we return the list with only ins
            (cons ins null)
            (if (> ins (car list))		;base case: if ins is greater of the first element of the list than we need to redo the recursion
                (cons 
      (car list) 
      (sorted-ins (cdr list) ins)	;recursive call
    )
                (cons ins list)			;correct position found
            )
        )
    )
  )
)

;The function sorted-list keep a list, and return the list sorted
(define sorted-list				;return a sorted list
  (lambda (list)				;list is a list
    (if (null? list)				;base case
        null
        (sorted-ins 				;we do the sorting with the sorted-ins functions:
    (sorted-list (cdr list)) 		;we use as a list a new void list
    (car list)				;and as the element to insert sorted the first element of our unsorted list
  )
    )
  )
)

;The function bubblesort keep a list, and return the list sorted

;NB: this function is written under the effect of a brain storming, the correct function of the exercise
; is sorted-list, but this idea is sexy and crazy, so i decided to save it; also probably is faster, so read it if you want to do this face O_O
; also, sort and sorted? are only support functions, written only because is easier to do the bubblesort in this way (yep, it is possibile without them, but
; this solution is good only if you are almost two times mad than me .-.)

(define bubblesort				;return a sorted list
  (lambda (list)				;list is a list
  (if (sorted? list) 				;bubble sort is an interactive algorithm, that use two annidated loops; to do this, with only the recursion
      list					; we need this control that simulate the loops, even if it isn't so efficient as the two loops
      (bubblesort (sort list))			;recursive call, sort is the function that the do the real bubble sort
  )
  )
)
;So the name bubblesort for this function isn't really good, this isn't the function that do the sorting, but only the function that control if the function
; is sorted or not, and if not to restart the sorting

;The function sort keep a list, and return an almost sorted list
(define sort					;return an almost sorted list
  (lambda (list)				;list is a list
    (if (null? (cdr list))			;base case; if the list is null, than we have probably sorted all the elements
        list
        (if (> (car list) (car (cdr list)))	;control if we need to do an exchange of the first two elements
            ;true branch
              (cons 				;if we need to do the exchange, than we need to it in a really tricky way
    (car (cdr list)) 		; the exchange is done building a new list, that begins with the second element of the unbubbled list
    (sort (cons 			; and then we recursive call the sort function, giving to it the list that is composed of
      (car list) 			; the first element of the list, and all the other elements, but not the second
      (cdr (cdr list))		; and to do this we need to remove the first element twice (little crazy, but it works :))
    ))
        )
            ;false branch
              (cons 				;we are lucky and we need no exchange: so we build the list using
    (car list) 			; the first element of the unbubbled list
    (sort (cdr list))		; and the recursive call of sort, using the unbubbled list without his first element
        )
        )
    )    
  )
)

;The function sorted? return true if a list is sorted, false if not
(define sorted?					;return a boolean
  (lambda (list)				;list is a list of integer
    (if (null? list)				;a list without any element is sorted :)
        true
        (if (> (car list) (car (cdr list)))	;if the first element is greater than the second the list is unsorted
            false
            (sorted? (cdr list))		;and if the first two elements are sorted and the rest of the list not? Better to check with a recursive call :)
        )
    )
  )
)

;End of the exercise \('ε')/