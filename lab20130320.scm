#lang scheme
(define gflavio						;return the last two knights
  (lambda (n)						;n is an integer
    (rotate (new-round-table n))
  )
)

(define new-round-table					;return a round table
  (lambda (n)						;n is an integer
    (list
    (round-table 1 n)					;the knights
    '()						;a pivot
    n							;the number of the knights
    )
  )
)

(define round-table					;return a list of knights
  (lambda (x y)						;x,y are integers
    (if (> x y)
        '()
        (cons x (round-table (+ x 1) y))
    )
  )
)

(define number-of-knights-in caddr)			;alias for caddr

(define rotate
  (lambda (tab)
    (let (
          (knights (number-of-knights-in tab))		;how much knights are still in the table?
          (table (car tab))				;list of knights
          (pivot (cadr tab))				;the pivot
          (n (- (number-of-knights-in tab) 1))		;the number of knights without the drunken one
          )
      (cond
        ((= 2 knights) (append table pivot))		;finished
        ( (null? table) (rotate (list pivot '() knights) ))
        ( (null? (cdr table)) (rotate (list (append (cddr pivot) (list (car table) (car pivot))) '() n)))
        ( (null? (cddr table)) (rotate (list (cdr (append pivot table)) '() n)))
        ( else (rotate (list (cdddr table) (append pivot (list (car table) (cadr table))) n)))
      )
    )
  )
)
