#lang scheme
;Pastrolin Alberto
;21/11/2012
;Many functions for the prime numbers

;The function prime? check if a number is prime or not

(define prime?													;return true if the number is prime, false if not
  (lambda (num)												;num is a natural number
    (if (and (> num 2) (= 0 (remainder num 2)))	;a par number isn't prime
      false													; so we can skip the control of this number
      (is_prime? num 
        (divisor_list (truncate (sqrt num)))
      )														;check if the number is prime or not
    )
  )
)

;The function divisor_list create a list of number, from the square rad of num and 2, without any par number

(define divisor_list											;return a list of natural number
  (lambda (num)												;num is a natural number
    (if (<= num 2)											;base case; we have finished to create the list
      null
      (cons num (divisor_list (- num 2)))			;recursive call that also build the list of divisors
    )
  )
)

;The function is_prime control effectively if a number is prime or not using the rest of a division
; (if the rest of num and another number is 0, than num isn't prime)

(define is_prime?												;return true if the number is prime, false if not
  (lambda (num divisors)									;num is a natural number, divisors is a list of prime numbers
    (if (null? divisors)									;base case; the number have resist to the list of divisors
      true													; so it is prime: good job num!
      (if (= (remainder num (car divisors)) 0)	;we haven't finished the divisors so we need to do another check
        false												;num can be divided, so it isn't a prime number :(
        (is_prime? num (cdr divisors))			;num can't be divided form this number, so better to check if
      )														; other divisors can divide it, or if it is a true prime number :)
    )
  )
)

;The function prime-factors factorize in prime numbers a number given in input

(define prime-factors										;return a list of natural number
  (lambda (num)												;num is a natural number
    (if (prime? num)										;if num is prime, his factorizzation is num
      (cons num '())
      (cons (great_divisor num) 						;if not, we search the first prime number that can divide him
        (prime-factors (/ num 						;and we do a recursive call to search into num/factor
          (great_divisor num))
        )
      )
    )
  )
)

;The function great_divisor return the greatest prime number that is a divisor of num

(define great_divisor										;return a natural number
  (lambda (num)												;num is a natural number
    (greatest num (divisor_list (- num 1)))
  )
)

;The function greatest return the greatest prime number of a list that can divide another given number

(define greatest												;return a natural number
  (lambda (num divisors)									;num is a natural number, divisors is a list of natural number
    (if 
      (and 
        (prime? (car divisors))						;if the number is prime
        (= 0 (remainder num (car divisors)))	;and can divide num, we return it
      )
      (car divisors)
      (greatest num (cdr divisors))					;else, we search for another number
    )
  )
)

;The function short-prime-factors return the prime factors of a number, without doubles

(define short-prime-factors								;return a sorted list of prime numbers
  (lambda (num)												;num is a natural number
    (sorted-list (short (prime-factors num)))
  )
)

;The function belong? return true if an element is in list and return false if not

(define belong?					;return a boolean
  (lambda (list element)			;list is a list, element is an integer
    (if (null? list)				;base case, if a list is null than element can't belong to list
        false
        (if (= (car list) element)
            true
            (belong? (cdr list) element)	;recursive call
        )
    )
  )
)

;The function sorted-ins insert an element in a sorted list, and return also a sorted list

(define sorted-ins				;return a sorted list
  (lambda (list ins)				;list is a sorted list, ins is the integer that we wont to insert in the list
    (if (belong? list ins)			;a quicker control to check if the element is just in the list (in this case the list is already sorted with ins)
        list
        (if (null? list)			;another quick control, if the list hasn't elements than we return the list with only ins
            (cons ins null)
            (if (> ins (car list))		;base case: if ins is greater of the first element of the list than we need to redo the recursion
                (cons 
      (car list) 
      (sorted-ins (cdr list) ins)	;recursive call
    )
                (cons ins list)			;correct position found
            )
        )
    )
  )
)

;The function sorted-list keep a list, and return the list sorted

(define sorted-list				;return a sorted list
  (lambda (list)				;list is a list
    (if (null? list)				;base case
        list
        (sorted-ins 				;we do the sorting with the sorted-ins functions:
    (sorted-list (cdr list)) 		;we use as a list a new void list
    (car list)				;and as the element to insert sorted the first element of our unsorted list
  )
    )
  )
)

;The function short return a list, without doubles

(define short									;return a list without doubles
  (lambda (elements)						;elements is a list
    (if (null? elements)					;null hasn't any double :)
      null
      (if
        (belong?
          (cdr elements)
          (car elements)
        )
        (short (cdr elements))		;we have find a double: see you!
        (cons								;no double, but better to re-check with a recursive call
          (car elements)
          (short (cdr elements))
        )
      )
    )
  )
)