#lang scheme
(define append
  (lambda (list1 list2)
    (if (null? list1)
        list2
        (cons (car list1) (append (cdr list1) list2))
    )
  )
)

(define reverse
  (lambda (list)
    (if (null? list)
        list
        (append (reverse (cdr list)) (cons (car list) null))
    )
  )
)