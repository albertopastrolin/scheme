#lang scheme
;Pastrolin Alberto
;28/11/2012
;Sum of two BTR number

(define btr-digit-sum                    ; val:     carattere +/./-
  (lambda (u v c)                        ; u, v, c: caratteri +/./-
    (cond ((char=? u #\-)                ; u v c
          (cond ((char=? v #\-)
                  (cond ((char=? c #\-)  ; - - -
                        #\.)
                        ((char=? c #\.)  ; - - .
                        #\+)
                        ((char=? c #\+)  ; - - +
                        #\-)))
                ((char=? v #\.)
                  (cond ((char=? c #\-)  ; - . -
                        #\+)
                        ((char=? c #\.)  ; - . .
                        #\-)
                        ((char=? c #\+)  ; - . +
                        #\.)))
                ((char=? v #\+)         ; - + c
                  c)))
          ((char=? u #\.)
          (cond ((char=? v #\-)
                  (cond ((char=? c #\-)  ; . - -
                        #\+)
                        ((char=? c #\.)  ; . - .
                        #\-)
                        ((char=? c #\+)  ; . - +
                        #\.)))
                ((char=? v #\.)         ; . . c
                  c)
                ((char=? v #\+)
                  (cond ((char=? c #\-)  ; . + -
                        #\.)
                        ((char=? c #\.)  ; . + .
                        #\+)
                        ((char=? c #\+)  ; . + +
                        #\-)))))
          ((char=? u #\+)
          (cond ((char=? v #\-)         ; + - c
                  c)
                ((char=? v #\.)
                  (cond ((char=? c #\-)  ; + . -
                        #\.)
                        ((char=? c #\.)  ; + . .
                        #\+)
                        ((char=? c #\+)  ; + . +
                        #\-)))
                ((char=? v #\+)
                  (cond ((char=? c #\-)  ; + + -
                        #\+)
                        ((char=? c #\.)  ; + + .
                        #\-)
                        ((char=? c #\+)  ; + + +
                        #\.)))))
          )))


(define btr-carry
  (lambda (first second carry)
    (cond
      ((and (char=? first #\+) (char=? second #\+) (not (char=? carry #\-))) #\+)
      ((and (char=? first #\+) (not (char=? second #\-)) (char=? carry #\+)) #\+)
      ((and (not (char=? first #\-)) (char=? carry #\+) (char=? second #\+)) #\+)
      ((and (char=? first #\-) (char=? second #\-) (not (char=? carry #\+))) #\-)
      ((and (char=? first #\-) (not (char=? second #\+)) (char=? carry #\-)) #\-)
      ((and (not (char=? first #\+)) (char=? carry #\-) (char=? second #\-)) #\-)
      (else #\.)
    )
  )
)

(define head
  (lambda (brt)
    (if (string=? brt "") ""
      (substring brt 0 (- (string-length brt) 1))
    )
  )
)

(define lsd
  (lambda (brt)
    (if (string=? brt "") #\.
      (string-ref brt (- (string-length brt) 1))
    )
  )
)

(define normalized-btr
  (lambda (brt)
    (if (and (> (string-length brt) 1) (char=? #\. (string-ref brt 0)))
      (normalized-btr (substring brt 1 (string-length brt)))
      brt
    )
  )
)

(define btr-carry-sum
  (lambda (first second carry)
    (if (and (string=? second "") (char=? carry #\.))
      (normalized-btr first)
      (string-append 
        (btr-carry-sum (head first) (head second) (btr-carry (lsd first) (lsd second) carry)) 
        (string (btr-digit-sum (lsd first) (lsd second) carry))
      )
    )
  )
)

(define btr-sum
  (lambda (first second)
    (btr-carry-sum first second #\.)
  )
)