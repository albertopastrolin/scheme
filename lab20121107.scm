#lang scheme
(define bin-rep->number ; numero
  (lambda (rap)        ; rappresentazione binaria
    (string-append
    (sign rap)
    (number->string (conversion (integer-part rap)))
    "."
    (number->string (conversion (float-part rap)))
    )
  )
)

(define rap-nosign
  (lambda (rap)
    (substring rap 1 (string-length rap))
  )
)

(define sign
  (lambda (rap)
    (if
    (string=? (first-char rap) "-")
      "-"
      ""
    )
  )
)

(define first-char
  (lambda (rap)
    (substring rap 0 1)
  )
 )
               
(define conversion
  (lambda (rap-nosign)
    (if
    (= (string-length rap-nosign) 0)
    0
    (+ (*
        (expt 2 (- (string-length rap-nosign) 1))
        (first-num rap-nosign)
        )
      (conversion (tail rap-nosign))
    )
  )
  )
)
(define first-num            ; 
  (lambda (rap-nosign)
    (if 
    (string=? (substring rap-nosign 0 1) "1") 1 0)
    ))
(define tail
  (lambda (rap-nosign)
    (substring rap-nosign 1 (string-length rap-nosign))
  )
 )
(define integer-part
  (lambda (rap-nosign)
    (let ((point-index (- (point rap-nosign) 1)))
    (if
    (> point-index 0) (substring rap-nosign 0 point-index)
    rap-nosign
     
))))
(define point
  (lambda (rap-nosign)
    (if
    (or (string=? rap-nosign "") (char=? (extract-last-char? rap-nosign) #\.))
    (string-length rap-nosign)
      (point (substring rap-nosign 0 (- (string-length rap-nosign) 1)))
      )))
      
(define extract-last-char?
 (lambda (rap-nosign)
  (string-ref rap-nosign (-(string-length rap-nosign) 1))
  )
)
(define float-part
  (lambda (rap-nosign)
    (let ((point-index (point rap-nosign)))
    (if
    (> point-index 0) (substring rap-nosign point-index (string-length rap-nosign))
    "0"
     
))))
