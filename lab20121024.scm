#lang scheme
(define singleDesinate                        ;return: string
  (lambda (verb)                              ;verb:   string
    (string-append " " (cond
      ((pred? verb "are") (replace verb "a"))
      (else (replace verb "e"))
    ))
  )
)

(define multiDesinate                         ;return: string
  (lambda (verb)                              ;verb:   string
    (string-append " " (cond
      ((pred? verb "are") (replace verb "ano"))
      (else (replace verb "ono"))
    ))
  )
)

(define pred?                                 ;return:      boolean
  (lambda (verb test)                         ;verb & test: string
    (string=? (tail verb) test)
  )
)

(define tail                                 ;return: string
  (lambda (verb)                             ;verb:   string
    (substring verb (- (string-length verb) 3) (string-length verb))
  )
)

(define replace                              ;return:         string
  (lambda (verb new_end)                     ;verb & new_end: string
    (string-append (substring verb 0 (- (string-length verb) 3)) new_end)
  )
)

(define phraseCreator			     ;return: string
  (lambda (subject verb object)		     ;subject & verb & object: string
    (string-append (subjectDesinator subject verb) (objectDesinator object))
  )
)

(define objectDesinator                      ;return: string
  (lambda (object)                           ;object: string
    (if (isMale? object)
        (if (isSingle? object)
            (string-append " il " object) ;single male
            (string-append " i "  object) ;multi male
        )
        (if (isSingle? object)    
            (string-append " la " object) ;single female
            (string-append " le " object) ;multi female
        )
    )
  )  
)

(define subjectDesinator                    ;return: string
  (lambda (subject verb)                    ;subject & verb: string
    (if (isMale? subject)
        (if (isSingle? subject)
            (string-append (string-append "Il " subject) (singleDesinate verb)) ;single male
            (string-append (string-append "I "  subject) (multiDesinate verb))  ;multi male
        )
        (if (isSingle? subject)    
            (string-append (string-append "La "  subject) (singleDesinate verb));single female
            (string-append (string-append "Le "  subject) (multiDesinate verb));multi female
        )
    )
  )  
)

(define isMale?                                 ;return: boolean
  (lambda (subject)                             ;subject: string
    (or
        (char=? (string-ref subject (- (string-length subject) 1)) #\o)
        (char=? (string-ref subject (- (string-length subject) 1)) #\i)
    )
  )
)

(define isSingle?                                ;return: boolean
  (lambda (subject)                              ;subject: string
    (or
        (char=? (string-ref subject (- (string-length subject) 1)) #\o)
        (char=? (string-ref subject (- (string-length subject) 1)) #\a)
    )
  )
)

;input

(define subject (read))
(define verb (read))
(define object (read))
(phraseCreator subject verb object)
