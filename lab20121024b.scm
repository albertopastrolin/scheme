#lang scheme
;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-intermediate-lambda-reader.ss" "lang")((modname puzzle) (read-case-sensitive #t) (teachpacks ((lib "tassellation.ss" "installed-teachpacks"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "tassellation.ss" "installed-teachpacks")))))

;; Square-Cross Puzzle
;; Claudio Mirolo, 27/10/2000

;; Per eseguire questo codice e' necessario
;; utilizzare il TeachPack "tassellation.ss"


;; Square-Cross Puzzle
;;
;; Hai a disposizione due forme base:
;;
;;   larger-tile
;;
;;   smaller-tile
;;
;; Puoi spostare e combinare le forme con le operazioni:
;;
;;   (shift-down <forma> <passi>)
;;
;;   (shift-right <forma> <passi>)
;;
;;   (quarter-turn-right <forma>)
;;
;;   (quarter-turn-left <forma>)
;;
;;   (half-turn <forma>)
;;
;;   (glue-tiles <forma> <forma>)
;;
;; dove  <forma>  e' una delle due forme base, eventualmente
;; spostata e/o ruotata,  <passi>  e' un numero naturale positivo
;; che rapresenta un'unita' di lunghezza minima dei possibili
;; spostamenti in giu' o a sinistra.
;; Le operazioni consentono, rispettivamente, di spostae in basso,
;; spostare a destra, ruotare di 90 gradi in senso orario, ruotare
;; di 90 gradi in senso antiorario, capovolgere e combinare
;; insieme due forme. ... Si capisce meglio provando!
;;
;; L'obiettivo e' di costruire una croce regolare e un quadrato
;; con il minimo numero di pezzi, corrispondenti alle forme base,
;; opportunamente posizionati e orientati.
;;
;; Quali altre figure si possono costruire?


;; Unit step

(define step-length 41)


;; Tile definitions

(set-default-image-size! (* 5 step-length))

(define larger-tile
  (overlay
    (overlay
      (overlay
        (overlay (filled-triangle  -4/5 1/5  -4/5 -3/5  0 -3/5)
                (filled-triangle  -4/5 1/5  0 -3/5  0 1/5))
        (filled-triangle  0 -1  4/5 3/5  0 1))
      (overlay
        (overlay (line  -4/5 1/5  -4/5 -3/5)
                (line  -4/5 -3/5  0 -3/5))
        (line  0 -3/5  0 -1))
      )
    (overlay
      (overlay (line  0 -1  4/5 3/5)
              (line  4/5 3/5  0 1))
      (overlay (line  0 1  0 1/5)
              (line  0 1/5  -4/5 1/5))
      )
    )
  )

(set-default-image-size! (* 3 step-length))

(define smaller-tile
  (overlay
    (overlay (filled-triangle  -2/3 -1/3  2/3 -1/3  -2/3 1/3)
            (line  -2/3 -1/3  2/3 -1/3))
    (overlay (line  2/3 -1/3  -2/3 1/3)
            (line  -2/3 1/3  -2/3 -1/3))
    )
  )


;; Shifting e Rotating Tiles

(define quarter-turn-left
  (lambda (tile)
    (quarter-turn-right
    (quarter-turn-right
      (quarter-turn-right tile)))
    )
  )

(define half-turn
  (lambda (tile)
    (quarter-turn-right
    (quarter-turn-right tile))
    )
  )

(define shift-down
  (lambda (tile steps)
    (stack (white-box (image-width tile) (* steps step-length))
          tile
          )
    )
  )

(define shift-right
  (lambda (tile steps)
    (let ((turned-tile (quarter-turn-right tile)))
      (quarter-turn-left
      (shift-down turned-tile steps))
      )
    )
  )


;; Gluing tiles

(define fill-down
  (lambda (tile height)
    (stack tile
          (white-box (image-width tile) height))
    )
  )

(define fill-right
  (lambda (tile width)
    (quarter-turn-right
    (stack (white-box (image-height tile) width)
            (quarter-turn-left tile))
    )
    )
  )

(define fill
  (lambda (tile width height)
    (let ((tw (image-width tile))
          (th (image-height tile)))
      (cond ((and (> width tw) (> height th))
            (fill-down
              (fill-right tile (- width tw))
              (- height th)))
            ((> width tw)
            (fill-right tile (- width tw)))
            ((> height th)
            (fill-down tile (- height th)))
            (else tile))
    )
    )
  )

(define glue-tiles
  (lambda (tile1 tile2)
    (let ((width
          (max (image-width tile1)
                (image-width tile2)))
          (height
          (max (image-height tile1)
                (image-height tile2)))
          )
      (overlay (fill tile1 width height)
              (fill tile2 width height))
      )
    )
  )


(glue-tiles
(glue-tiles
(shift-down (glue-tiles
larger-tile     
(shift-down (shift-right (half-turn larger-tile) 2) 1)) 1)
(shift-right (half-turn smaller-tile) 2))
(shift-down (shift-right smaller-tile 2) 5))