#lang scheme
;Pastrolin Alberto
;19/12/2012

(define f
  (lambda (n)
    (/ 
      (den (- n 1)) 
      (num (+ n 1)) 
    )
  )
)

(define den
  (lambda (n)
    (if (= n 0)
      1
      (+ (num (- n 1)) (num n))
    )
  )
)

(define num
  (lambda (n)
    (if (= n 0)
      1
      (den (- n 1))
    )
  )
)

(define g
  (lambda (n k)
    1
  )
)

(define h
  (lambda (n)
    1
  )
)

(define t
  (lambda (n)
    1
  )
)


(define f1
  (lambda (n)
    (if (= n 0)
      1
      (/ 1 (+ 1 (f1 (- n 1) ) ) )
    )
  )
)

(define g1
  (lambda (n k)
    (if (= k 0)
      1
      (quotient (* n (g1 (- n 1) (- k 1) ) ) k) 
    )
  )
)

(define h1
  (lambda (n)
    (if (= n 1)
      1
      (+ (* 2 (h1 (- n 1) ) ) n)
    )
  )
)

(define t1
  (lambda (n)
    (if (= n 1)
      1
      (+ (* 2 (t1 (quotient n 2) ) ) n)
    )
  )
)