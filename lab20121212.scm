#lang scheme
;Pastrolin Alberto
;12/12/2012

(define manhattan
  (lambda (row col)
    (if
      (or (= row 0) (= col 0))
      1
      (+ 
        (manhattan (- row 1) col) 
        (manhattan row (- col 1))
      )
    )
  )
)

(define manhattan-3d
  (lambda (row col pro)
    (cond
      ((= row 0) (manhattan pro col))
      ((= col 0) (manhattan row pro))
      ((= pro 0) (manhattan row col))
      (else	
        (+ 
          (manhattan-3d (- row 1) col pro) 
          (manhattan-3d row (- col 1) pro) 
          (manhattan-3d row col (- pro 1))
        )
      )
    )
  )
)

