#lang scheme
(require "tassellation.ss")
;; Rompicapo delle "n regine"
;;
;; Ultimo aggiornamento: 25/03/2013


;; Realizzazione del dato astratto "scacchiera"

(define empty-board             ; scacchiera vuota n x n
  (lambda (n)
    (list
    n                          ; 1) dimensione scacchiera
    0                          ; 2) numero regine collocate sulla scacchiera
    (lambda (u v) false)       ; 3) predicato: posizione <u,v> sotto scacco?
    (chessboard-image n)                         ; 4) descrizione della scacchiera
    )
    ))

(define size car)               ; dimensione scacchiera

(define queens-on cadr)         ; numero regine collocate sulla scacchiera

(define under-attack?           ; e' sotto scacco la posizione <i,j> ?
  (lambda (board i j)           ; i, j in [1, n]  per n = (size board)
    ((caddr board) i j)
    ))

(define add-queen               ; valore: scacchiera con una nuova regina
  (lambda (board i j)           ;         in posizione <i,j>
    (list
    (car board)                ; dimensione
    (+ (cadr board) 1)         ; numero regine
    (lambda (u v)              ; elemento procedurale: posizioni <u,v>, <i,j>
      (or (= u i)              ; sulla stessa riga
          (= v j)              ; sulla stessa colonna
          (= (- u v) (- i j))  ; sulla stessa diagonale \
          (= (+ u v) (+ i j))  ; sulla stessa diagonale /
          ((caddr board) u v)  ; posizione <u,v> gia' sotto scacco in board
          ))                   ; fine lambda-espressione
        (add-queen-image i j (size board) (arrangement board))
    ) ; aggiornamento della descrizione
    )
    )

(define arrangement cadddr)     ; descrizione della scacchiera

(define random-queen
  (lambda (n)
    (casual-board n (empty-board n))
  )
)

(define casual-board
  (lambda (n board)
    (if (= n (queens-on board))
          (arrangement board)
          (let ((i (+ (random n) 1)) (j (+ (random n) 1)))
            (if (under-attack? board i j)
                (casual-board n (empty-board n))
                (casual-board n (add-queen board i j))
            )
          )
    )
  )
)

;parte grafica
;; Unit step

(define step-length 28)


;; Tile definitions

(set-default-image-size! (* 5 step-length))


(define chessboard-image
  (lambda (n)
    (overlay
    (squares n 0 0)
    (overlay 
      (overlay 
      (line  -1 -1  1 -1)
      (line  1 -1  1 1))
      (overlay 
      (line  1 1  -1 1)
      (line  -1 1  -1 -1))
      ))
    ))


(define add-queen-image
  (lambda (i j n board)
    (overlay
    board
    (queen n (- j 1) (- n i))
    )
    ))


(define squares
  (lambda (n i j)
    (if (and (>= i (- n 2)) (= (+ j 1) n))
        (shaded-square n i j)
        (overlay
        (shaded-square n i j)
        (if (< (+ i 2) n)
            (squares n (+ i 2) j)
            (squares n (remainder (+ j 1) 2) (+ j 1))))
        )
    ))


(define shaded-square
  (lambda (n i j)
    (let ((x (- (/ (* 2 i) n) 1))
          (y (- (/ (* 2 j) n) 1))
          (d (/ 2 n))
          )
      (overlay
      (filled-triangle  x y  (+ x d) y  (+ x d) (+ y d))
      (filled-triangle  x y  (+ x d) (+ y d)  x (+ y d))
      ))
    ))


(define queen
  (lambda (n i j)
    (let ((x (+ (- (/ (* 2 i) n) 1) (/ 1 (* 2 n))))
          (y (+ (- (/ (* 2 j) n) 1) (/ 1 (* 2 n))))
          (d (/ 1 n))
          )
      (overlay
      (overlay
        (overlay
        (overlay
          (line  (+ x (/ d 4)) y  x (+ y d))
          (line  x (+ y d)  (+ x (/ d 4) (/ d 2)) y))
        (line  (+ x (/ d 2)) y  x (+ y d)))
        (overlay
        (line  (+ x (/ d 4)) y  (+ x (/ d 2)) (+ y d))
        (line  (+ x (/ d 2)) (+ y d)  (+ x (/ d 4) (/ d 2)) y)))
      (overlay
        (overlay
        (overlay
          (line  (+ x (/ d 2)) y  (+ x (/ d 2)) (+ y d))
          (line  (+ x (/ d 4)) y  (+ x d) (+ y d)))
        (line  (+ x d) (+ y d)  (+ x (/ d 4) (/ d 2)) y))
        (overlay
        (line  (+ x (/ d 2)) y  (+ x d) (+ y d))
        (line  (+ x (/ d 4)) y  (+ x (/ d 4) (/ d 2)) y)))
      ))
    ))
